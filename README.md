# React js Assessment

API URL: https://test.magicstack.dev/api/

### API Documentation

[Documentation](/API_DOCS.md)

This Documentation contains API routes, Method(POST,GET,etc.) & Responses


### Using the Above API documentation Build An Android App with following features.

- Register
- Login
- Refresh Login Token
- View Own Profile 
- Update Profile details
- Upload Images
- List of Users With Images
- Show User Details

## IMP NOTES: 
- After completing the task create public repository in GitHub and push the code.
- don't send zip file or attachment to email.
- If you don't know about git then you are not eligible. 
- Code should be well formatted 
- Handle all Errors and Exceptions with your own knowledge
- Add screenshots or video of your app in repository
